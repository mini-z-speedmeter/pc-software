#include "maindisplaywidget.h"
#include "ui_maindisplaywidget.h"


#include <QEvent>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QMenu>
#include <QMessageBox>

MainDisplayWidget::MainDisplayWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainDisplayWidget)
{
    ui->setupUi(this);
    this->setWindowTitle(QStringLiteral("MiniZ Speed-O-Meter by Pila"));

    //Disable the maximize button to avoid interfering with fullscreen mode
    setWindowFlags(windowFlags()&(~Qt::WindowMaximizeButtonHint));

    serialInterface = new SerialInterface(this);

    settingsDialog = new SettingsDialog(this);

    connect(settingsDialog,&SettingsDialog::newSerialSettingsToApply,serialInterface,&SerialInterface::connectSerialPort);
    connect(settingsDialog,&SettingsDialog::newGuiSettingsToApply,this,&MainDisplayWidget::setGuiSettings);
    connect(serialInterface,&SerialInterface::serialErrorOccured,this,&MainDisplayWidget::displayCriticalMessage);
    connect(serialInterface,&SerialInterface::speedReceived,this,&MainDisplayWidget::processNewSpeed);

    //this queued connection makes the dialog only open after maindisplaywidget is shown
    connect(this,&MainDisplayWidget::widgetLoaded,settingsDialog,&SettingsDialog::exec, Qt::ConnectionType(Qt::QueuedConnection | Qt::UniqueConnection));

    buildAndEnableContextMenu();

    ui->centralWidget->setCaption(tr("Speed (km/h)"));
    ui->leftWidget->setCaption(tr("Highest"));
    ui->bottomWidget->setCaption(tr("Previous"));
    ui->rightWidget->setCaption(tr("Average"));

    maxNumberOfValueForAveraging = 0;

    resetDisplays();
}

void MainDisplayWidget::buildAndEnableContextMenu(){
    toggleFullScreenAction = new QAction(tr("Full screen") , this);
    toggleFullScreenAction->setCheckable(true);
    toggleFullScreenAction->setChecked(false);
    connect(toggleFullScreenAction,&QAction::toggled,this,&MainDisplayWidget::setFullScreen);

    portSetupAction = new QAction(tr("Settings") , this);
    connect(portSetupAction,&QAction::triggered,settingsDialog,&SettingsDialog::exec);

    quitAction = new QAction(tr("Quit") , this);
    connect(quitAction,&QAction::triggered,this,&MainDisplayWidget::close);

    resetDisplaysAction = new QAction(tr("Reset displays") , this);
    connect(resetDisplaysAction,&QAction::triggered,this,&MainDisplayWidget::resetDisplays);


    this->addAction(toggleFullScreenAction);
    this->addAction(resetDisplaysAction);
    this->addAction(portSetupAction);
    this->addAction(quitAction);

    this->setContextMenuPolicy(Qt::ActionsContextMenu);
}

void MainDisplayWidget::resetDisplays(){
    highestSpeed=-1;
    previousSpeed=-1;
    measureNumber=0;
    averageSpeed=0;
    ui->centralWidget->displaySpeed(-1);
    ui->bottomWidget->displaySpeed(-1);
    ui->leftWidget->displaySpeed(-1);
    ui->rightWidget->displaySpeed(-1);
}

void MainDisplayWidget::setGuiSettings(SettingsDialog::GuiSettings settings){
    maxNumberOfValueForAveraging=settings.averagingValues;
    ui->centralWidget->setFlashEffectParameters(settings.flashEffectEnabled,
                                                settings.flashDuration,
                                                settings.flashColor);
    ui->bottomWidget->setFlashEffectParameters(settings.flashEffectEnabled,
                                               settings.flashDuration,
                                               settings.flashColor);
    ui->leftWidget->setFlashEffectParameters(settings.flashEffectEnabled,
                                             settings.flashDuration,
                                             settings.flashColor);
    ui->rightWidget->setFlashEffectParameters(settings.flashEffectEnabled,
                                              settings.flashDuration,
                                              settings.flashColor);
}

void MainDisplayWidget::displayCriticalMessage(QString message){
    QMessageBox::critical(this,windowTitle(),message);
}

void MainDisplayWidget::processNewSpeed(double speed){

    //Speed
    ui->centralWidget->displaySpeed(speed);

    //Previous speed
    ui->bottomWidget->displaySpeed(previousSpeed);
    previousSpeed=speed;

    //Highest speed
    if(speed>highestSpeed){
        highestSpeed=speed;
        ui->leftWidget->displaySpeed(highestSpeed);
    }

    //Average speed
    if(measureNumber>maxNumberOfValueForAveraging)measureNumber=maxNumberOfValueForAveraging;
    averageSpeed = ((averageSpeed*measureNumber)+speed);
    averageSpeed /= ++measureNumber;
    ui->rightWidget->displaySpeed(averageSpeed);
}

void MainDisplayWidget::showEvent(QShowEvent *event){
    QWidget::showEvent(event);
    static int cpt = 0;
    if(cpt == 0 ){
        emit widgetLoaded();
        cpt = 1;
    }
}

void MainDisplayWidget::keyPressEvent(QKeyEvent* event){
    //if(event->key()==Qt::Key_D)processNewSpeed(qrand()*100/RAND_MAX);
    if(isFullScreen()){
        event->accept();
        setFullScreen(false);
    }
}

void MainDisplayWidget::mouseDoubleClickEvent(QMouseEvent *event){
    Q_UNUSED(event);
    setFullScreen(!isFullScreen());
}

void MainDisplayWidget::setFullScreen(bool full){
    if(full)    this->showFullScreen();
    else        this->showNormal();
    toggleFullScreenAction->setChecked(this->isFullScreen());
}

MainDisplayWidget::~MainDisplayWidget()
{
    delete ui;
}

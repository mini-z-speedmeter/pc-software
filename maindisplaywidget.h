#ifndef MAINDISPLAYWIDGET_H
#define MAINDISPLAYWIDGET_H

#include "settingsdialog.h"
#include "serialinterface.h"

#include <QWidget>
#include <QAction>

namespace Ui {
class MainDisplayWidget;
}

class MainDisplayWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainDisplayWidget(QWidget *parent = 0);
    ~MainDisplayWidget();

signals:
    void widgetLoaded();

private slots:
    void setFullScreen(bool full);
    void displayCriticalMessage(QString message);
    void processNewSpeed(double speed);
    void resetDisplays(void);
    void setGuiSettings(SettingsDialog::GuiSettings settings);

protected:
    void showEvent(QShowEvent* event);
    void keyPressEvent(QKeyEvent* event);
    void mouseDoubleClickEvent(QMouseEvent * event);

private:
    void buildAndEnableContextMenu();


private:
    QAction* toggleFullScreenAction;
    QAction* resetDisplaysAction;
    QAction* portSetupAction;
    QAction* quitAction;

    Ui::MainDisplayWidget *ui;

    SettingsDialog* settingsDialog;
    SerialInterface* serialInterface;

    double highestSpeed;
    double previousSpeed;
    double averageSpeed;
    unsigned int measureNumber;
    unsigned int maxNumberOfValueForAveraging;
};

#endif // MAINDISPLAYWIDGET_H

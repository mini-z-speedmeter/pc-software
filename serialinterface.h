#ifndef SERIALINTERFACE_H
#define SERIALINTERFACE_H

#include <QObject>
#include <QSerialPort>
#include <QString>
#include <QByteArray>

#include "settingsdialog.h"

class SerialInterface : public QObject
{
    Q_OBJECT
public:
    explicit SerialInterface(QObject *parent = 0);

signals:
    void serialErrorOccured(QString error);
    void lowBattery(bool low);
    void speedReceived(double speed);
    void speedError();

public slots:
    void connectSerialPort(SettingsDialog::SerialSettings settings);

private slots:
    void readSerialPort();
    void handleError(QSerialPort::SerialPortError error);

private:
    QSerialPort* serialPort;
    QByteArray receivedBytes;
    bool lastDataReceivedIsLowBattery;
};

#endif // SERIALINTERFACE_H

#ifndef SPEEDDISPLAY_H
#define SPEEDDISPLAY_H

#include <QWidget>
#include <QPainter>
#include <QString>
#include <QTimer>
#include <QColor>


class SpeedDisplay : public QWidget
{
    Q_OBJECT
public:
    explicit SpeedDisplay(QWidget *parent = 0);

signals:

public slots:
    void setCaption(QString newCaption);
    void displaySpeed(double speed);
    void setFlashEffectParameters(bool enabled, unsigned int duration = 500, QColor color = Qt::yellow);

protected:
    void paintEvent(QPaintEvent* event);

private:
    void flash();
    void unflash();
    QString caption;
    QString value;
    QColor flashColor;
    QTimer* flashTimer;

    bool flashEnabled;
    bool flashed;
};

#endif // SPEEDDISPLAY_H

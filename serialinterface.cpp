#include "serialinterface.h"

#define CHAR_SEPARATOR      '\n'

SerialInterface::SerialInterface(QObject *parent) :
    QObject(parent)
{
    serialPort = new QSerialPort(this);
    connect(serialPort,&QSerialPort::readyRead,this,&SerialInterface::readSerialPort);
    connect(serialPort,SIGNAL(error(QSerialPort::SerialPortError)),this,SLOT(handleError(QSerialPort::SerialPortError)));

    lastDataReceivedIsLowBattery = false;
}


void SerialInterface::connectSerialPort(SettingsDialog::SerialSettings settings){
    if(serialPort->isOpen())    serialPort->close();

    serialPort->setPortName(settings.portName);
    serialPort->setBaudRate(settings.baudRate);
    serialPort->setDataBits(settings.dataBits);
    serialPort->setParity(settings.parity);
    serialPort->setStopBits(settings.stopBits);
    serialPort->setFlowControl(settings.flowControl);

    if (!serialPort->open(QIODevice::ReadOnly)) {
        emit serialErrorOccured(tr("Unable to open port %1 \n%2").arg(settings.portName).arg(serialPort->errorString()));
    }
}

void SerialInterface::readSerialPort(){
    receivedBytes.append(serialPort->readAll());

    //Separate each line except the last one if not finished
    QList<QByteArray> receivedLines=receivedBytes.split(CHAR_SEPARATOR);
    receivedBytes=receivedLines.takeLast();

    //Parse each line
    QByteArray line;
    foreach(line, receivedLines){
        line.replace("\r",QByteArray());

        if(line == "low battery") {
            lastDataReceivedIsLowBattery=true;
            emit lowBattery(true);
        }
        else{
            lastDataReceivedIsLowBattery=false;
            if(!lastDataReceivedIsLowBattery) emit lowBattery(false);

            bool conversionOk;
            double speed = line.toDouble(&conversionOk);

            if(conversionOk){
                emit speedReceived(speed);
            }
            else if(line == "slow") {
                emit speedReceived(0.0);
            }
            else if(line == "error"){
                emit speedError();
            }
        }
    }
}


void SerialInterface::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        emit serialErrorOccured(tr("Critical Error \n%1").arg(serialPort->errorString()));
        serialPort->close();
    }
}

#-------------------------------------------------
#
# Project created by QtCreator 2014-11-23T18:42:01
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MiniZSpeedOMeter
TEMPLATE = app


SOURCES += main.cpp\
    maindisplaywidget.cpp \
    settingsdialog.cpp \
    serialinterface.cpp \
    speeddisplay.cpp

HEADERS  += \
    maindisplaywidget.h \
    settingsdialog.h \
    serialinterface.h \
    speeddisplay.h

FORMS    += \
    maindisplaywidget.ui \
    settingsdialog.ui

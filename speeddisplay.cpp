#include "speeddisplay.h"

SpeedDisplay::SpeedDisplay(QWidget *parent) :
    QWidget(parent)
{
    flashEnabled =true;

    flashed=false;
    flashTimer = new QTimer(this);

    connect(flashTimer,&QTimer::timeout,this,&SpeedDisplay::unflash);
}

void SpeedDisplay::setCaption(QString newCaption){
    caption=newCaption;
    update();
}

void SpeedDisplay::setFlashEffectParameters(bool enabled, unsigned int duration, QColor color){
    flashEnabled=enabled;
    flashTimer->setInterval(duration);
    flashColor=color;
}

void SpeedDisplay::displaySpeed(double speed){
    if(speed == 0.0 ){
        value=tr("Slow !");
        flash();
    }
    else if (speed >0.0 ){
        value=QString::number(speed,'f',1);
        flash();
    }
    else{
        value.clear();
    }
    update();
}

void SpeedDisplay::flash(){
    if(flashEnabled){
        flashed = true;
        flashTimer->start();
    }
}

void SpeedDisplay::unflash(){
    flashed = false;
    update();
}

void SpeedDisplay::paintEvent(QPaintEvent *event){
    Q_UNUSED(event);
    QPainter painter(this);


    painter.setPen(Qt::white);

    int captionValueHeightLimit = height()*0.25;
    if(!caption.isEmpty()){
        QRect boundingRect(0,0,width(),captionValueHeightLimit);
        float factor = qMin(boundingRect.width() / painter.fontMetrics().width(caption),
                            boundingRect.height() / painter.fontMetrics().height());
        if(factor < 1.0 || factor > 1.25){
            QFont f = painter.font();
            f.setPointSizeF(f.pointSizeF()*factor);
            painter.setFont(f);
        }
        painter.drawText(boundingRect,Qt::AlignCenter,caption);
    }

    if(flashed && flashEnabled){
        painter.setPen(flashColor);
    }

    if(!value.isEmpty()){
        QRect boundingRect(0,captionValueHeightLimit,width(),height()-captionValueHeightLimit);
        float factor = qMin(boundingRect.width() / painter.fontMetrics().width(value),
                            boundingRect.height() / painter.fontMetrics().height());
        if(factor < 1.0 || factor > 1.25){
            QFont f = painter.font();
            f.setPointSizeF(f.pointSizeF()*factor);
            painter.setFont(f);
        }
        painter.drawText(boundingRect,Qt::AlignCenter,value);
    }
    painter.end();
}

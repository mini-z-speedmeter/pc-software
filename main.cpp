
#include "maindisplaywidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setStyleSheet(QStringLiteral("MainDisplayWidget {background: black}"));

    MainDisplayWidget mainWidget;
    mainWidget.showNormal();

    return a.exec();
}
